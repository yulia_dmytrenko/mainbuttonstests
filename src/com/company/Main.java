package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class Main {

  public String baseURL = "https://mister.am";
  public String facebookURL = "https://www.facebook.com/mister.am.chernihiv";
  public String instagramURL = "https://www.instagram.com/mister.am.chernihiv/";
  public String dotsPlatformURL = "https://dotsplatform.com/?utm_source=ma&utm_medium=link&utm_campaign=accounts_website";
  public String googlePlayURL = "https://play.google.com/store/apps/details?id=am.mister.misteram";
  public String appStoreURL = "https://apps.apple.com/app/apple-store";
  public String rulesURL = "https://docs.google.com/presentation/d/1eqd2N3GeABjGgQ7mS6z0DAqKce6CrQzzfLR-__aiubM/pub?start=false&loop=false&delayms=3000&slide=id.p";
  public WebDriver webDriver;

  @BeforeTest
  public void initBrowser() {
    System.setProperty("webdriver.chrome.driver", "D:\\selenium\\chromedriver.exe");
    webDriver = new ChromeDriver();
    webDriver.get(baseURL);
  }


  @Test
  public void googlePlayLink() throws InterruptedException {
    WebElement googlePlay = webDriver.findElement(By.xpath("//div[@class='bl-link-apps']/a[1]"));
    googlePlay.click();
    ArrayList<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
    webDriver.switchTo().window(tabs.get(1));
    if (webDriver.getCurrentUrl().contains(googlePlayURL))
      System.out.println("Google Play page is opened");
    else
      System.out.println("Google Play page is not opened");
  }

  @Test
  public void appStoreLink() {
    WebElement appStore = webDriver.findElement(By.xpath("//div[@class='bl-link-apps']/a[2]"));
    appStore.click();
    ArrayList<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
    webDriver.switchTo().window(tabs.get(1));
    if (webDriver.getCurrentUrl().contains(appStoreURL))
      System.out.println("App store page is opened");
    else
      System.out.println("app store page is not opened");
  }


  @Test
  public void checkingRestaurantList() {
    WebElement restaurantList = webDriver.findElement(By.xpath("//div[@class='main-button-container']/input"));
    restaurantList.click();
    System.out.println(webDriver.findElement(By.className("active")).isDisplayed());
  }

  @Test
  public void facebookLink() {
    WebElement facebook = webDriver.findElement(By.xpath("//div[@class='social']/span[2]"));
    facebook.click();
    if (webDriver.getCurrentUrl().equalsIgnoreCase(facebookURL))
      System.out.println("Facebook page is opened");
    else
      System.out.println("Facebook page is not opened");
  }

  @Test
  public void instagramLink() {
    WebElement instagram = webDriver.findElement(By.xpath("//div[@class='social']/span[1]"));
    instagram.click();
    if (webDriver.getCurrentUrl().equalsIgnoreCase(instagramURL))
      System.out.println("Instagram page is opened");
    else
      System.out.println("Instagram page is not opened");
  }

  @Test
  public void stocksLink() {
    WebElement stocks = webDriver.findElement(By.xpath("//div[@class='links']/a[1]"));
    stocks.click();
    if (webDriver.getCurrentUrl().contains("promotions"))
      System.out.println("Promotions page is opened");
    else
      System.out.println("Promotions page is not opened");
  }

  @Test
  public void rulesLink() {
    WebElement rules = webDriver.findElement(By.xpath("//div[@class='links']/a[2]"));
    rules.click();
    ArrayList<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
    webDriver.switchTo().window(tabs.get(1));
    if (webDriver.getCurrentUrl().equalsIgnoreCase(rulesURL))
      System.out.println("Rules page is opened");
    else
      System.out.println("Rules page is not opened");
  }

  @Test
  public void dotsPlatformLink() {
    WebElement dotsPlatform = webDriver.findElement(By.xpath("//div[@class='powered-by-section']/p/a"));
    dotsPlatform.click();
    if (webDriver.getCurrentUrl().equalsIgnoreCase(dotsPlatformURL))
      System.out.println("DotsPlatform page is  opened");
    else
      System.out.println("DotsPlatform page is not opened");
  }

  @AfterTest
  public void discontinueBrowser() {
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    webDriver.close();
  }

}
